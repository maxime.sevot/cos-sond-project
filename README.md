Projet de L3 MIAGE 2022 - 2023

# Cos'Sond Project

L'équipe des bosses : Emma OTTMANN, Corentin LEGEAY, Tom DURBOURJAL, Maxime SEVOT

# Pour utiliser git

git clone https://gitlab.com/maxime.sevot/cos-sond-project.git
cd cos-sond-project

Une fois dans le projet : 
Créer une branche pour le développement
Branche main pour la version principale (celle qui doit fonctionner toujours)
et 4 autres branches : 
    - emma
    - tom
    - corentin
    - maxime

Pour la créer : $ git checkout -b <name> (auto redirection vers la branche)
pour changer de branche : $ git switch <branch_name>

Pour actualliser les données d'une branche : $ git pull origin <branch_name> (souvent se sera main)
Pour fusionner à la main des changements : $ git merge orign <branch_name> (attention cela va surement créer des conflits) (pas recommandé)

# Pour valider le travail :
git add .
git commit -am 'bite' (Mettez des noms de commit explicite, même si c'est chiant, ça permettra de facilement récupérer un travail corompu)
git push origin <branch_name> (la vôtre de préférence) git push devrait fonctionner si vouys êtes dans votre branche

Ne pas oublier les gits pull avant de démarrer 

# Utilisation de Docker :

Le Dockerfile permet de créer notre propre image (pas d'utilité directe)
----- Pour l'architecte système ------
docker build -t <image_name>
docker run -p <port_local>:<port_distant>
-- Fin --------------------------------
le docker-compose.yml va servir à avoir tous le même conteneur.
Pour l'allumer : $ docker-compose up
Pour l'éteindre crtl + c
Pour supprimer le conteneur : $ docker-compose down

# Pour utiliser Postgres

Une fois votre docker-compose lancé, vous avec un nouveau container nommé cos-sond-project avec dedans un container postgres et adminer (vous pouvez taper localhost:8080 pour accéder à l'interface en ligne : user : miage, psw : miage, db : COSSOND)

Pour utiliser postgres lancez : $ docker exec -it postgres-miage /bin/bash
Dorénavant vous êtes sur un shell classique.
Allez dans /home/ et vous trouverez les fichiers du répertoire db (Si vous modifiez les scripts sur votre pc en local, la modification sera instantané dans le container (j'ai galéré à trouver comment faire 😅))

Connexion en faisiant : $ psql -U miage COSSOND
c'est bon vous êtes un boss 
pour lancer un script : $ \i <script>.sql

# Pour lancer le projet après un git pull
symfony composer install
npm i
npm install file-loader@^6.0.0 --save-dev
npm run dev watch
'supprimer la bd si elle est existante'
symfony console make:migration
symfony console d:m:m
symfony server:start
       OU
symfony serve -d
