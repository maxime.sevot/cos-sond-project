<?php

namespace App\Form;

use App\Entity\Statistiques;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StatistiquesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('requete', ChoiceType::class, [
                'choices' => [
                    'Réponse la plus repondu' => "input",
                    'Réponse la moins répondu' => "qcs",
                    'QCM' => "qcm",
                    'Date' => "date",
                    'Note' => "note",
                    "Nombre" => "nombre"],
                'placeholder' => 'Choisissez une réponse',
                'required' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Statistiques::class,
        ]);
    }
}
