<?php

namespace App\Form;

use App\Entity\Question;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('estOptionnelle')
            ->add('typeQuestion', ChoiceType::class, [
                'choices' =>
                    ['Question libre' => "input",
                    'QCS' => "qcs",
                    'QCM' => "qcm",
                    'Date' => "date",
                    'Note' => "note",
                    "Nombre" => "nombre"]
            ])
            ->add('titreQuestion')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Question::class,
        ]);
    }
}
