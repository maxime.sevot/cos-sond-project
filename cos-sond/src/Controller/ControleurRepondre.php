<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Entity\QuestionRepondue;
use App\Entity\Sondage;
use App\Repository\QuestionRepondueRepository;
use App\Repository\ReponseRepository;
use App\Repository\SondageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ControleurRepondre extends AbstractController
{
    #[IsGranted("ROLE_PARTICIPANT")]
    #[Route('/repondre', name: 'app_controleur_repondre')]
    public function index(): Response
    {
        $sondage = new Sondage();
        return $this->redirectToRoute('app_controller_home');
    }

    #[IsGranted("ROLE_PARTICIPANT")]
    #[Route('/repondre/liste_des_sondages/{id}', name: 'app_controleur_sondages_produits_nonrepondu')]
    public function liste_sondages_non_repondues_par_produit_par_utilisateur(Produit $produit): Response
    {
        $listeSondageNonReponduParProduit = $produit->getUnansweredSondages($this->getUser());
        return $this->render('controleur_repondre/index.html.twig', [
            'sondages' => $listeSondageNonReponduParProduit,
        ]);
    }


    #[Route('/repondre/{id}', name: 'app_controleur_repondre_sondage')]
    public function repondreSondage(Sondage $sondage): Response
    {
        return $this->render('controleur_repondre/repondre_sondage.html.twig', [
            'sondage' => $sondage,
            'questions' => $sondage->getQuestions(),
        ]);
    }

    #[IsGranted("ROLE_PARTICIPANT")]
    #[Route('/repondre/{id}/reponse', name: 'app_controleur_repondre_sondage_reponse')]
    public function repondre_au_sondage(Request $request , Sondage $sondage, ReponseRepository $reponseRepository, QuestionRepondueRepository $questionRepondueRepository): Response
    {
        $aujourdhui = new \DateTime();
        if ($aujourdhui <= $sondage->getDateFin()) {
            $formData = $request->request->all();
            $user = $this->getUser();
            foreach (array_keys($formData) as $key) {
                $reponse = $reponseRepository->find(explode("-", $key)[0]);
                if (gettype($formData[$key]) == 'array') {
                    foreach ($formData[$key] as $item) {
                        $repondreQuestion = new QuestionRepondue();
                        $repondreQuestion->setUtilisateur($user);
                        $repondreQuestion->setValeur($item);
                        $repondreQuestion->setReponse($reponse);
                        $questionRepondueRepository->save($repondreQuestion, true);
                    }
                } else {
                    if (!empty($formData[$key])) {
                        $repondreQuestion = new QuestionRepondue();
                        $repondreQuestion->setUtilisateur($user);
                        $repondreQuestion->setValeur($formData[$key]);
                        $repondreQuestion->setReponse($reponse);
                        $questionRepondueRepository->save($repondreQuestion, true);
                    }
                }
            }
            $this->addFlash('success', 'Vos réponses ont bien été prises en compte. Merci');
        } else {
            $this->addFlash('danger', 'Vous ne pouvez plus répondre à ce sondage (Temps dépassé)');
        }
        return $this->redirectToRoute("app_controleur_repondre");
    }

}
