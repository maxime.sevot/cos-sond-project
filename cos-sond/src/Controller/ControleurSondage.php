<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Entity\Sondage;
use App\Form\SondageType;
use App\Repository\QuestionRepository;
use App\Repository\SondageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

#[Route('/sondage')]
class ControleurSondage extends AbstractController
{

    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    #[IsGranted("ROLE_ADMIN")]
    #[Route('/', name: 'app_controleur_sondage_index', methods: ['GET'])]
    public function index(SondageRepository $sondageRepository): Response
    {
        $user = $this->getUser();
        return $this->render('controleur_sondage/index.html.twig', [
            'sondages' => $sondageRepository->findAll(),
        ]);
    }

    #[IsGranted("ROLE_SONDEUR")]
    #[Route('/new/{id}', name: 'app_controleur_sondage_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SondageRepository $sondageRepository, Produit $produit): Response
    {
        try {
            $sondage = new Sondage();
            $sondage->setCreateur($this->getUser());
            $today = new \DateTime();
            $sondage->setDateCreation($today);
            $sondage->setProduit($produit);
            $sondage->setEstPublie(false);

            $form = $this->createForm(SondageType::class, $sondage);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {

                $sondageRepository->save($sondage, true);

                return $this->redirectToRoute('app_controller_produit_index', [], Response::HTTP_SEE_OTHER);
            }

            return $this->render('controleur_sondage/new.html.twig', [
                'sondage' => $sondage,
                'form' => $form,
                'produit' => $produit,
            ]);
        }
        catch(\Exception $exception){
            $exception -> getTrace();
            return $this->redirectToRoute('app_login');
        }

    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SONDEUR')")
     */
    #[Route('/{id}', name: 'app_controleur_sondage_show', methods: ['GET'])]
    public function show(Sondage $sondage): Response {
        return $this->render('controleur_sondage/show.html.twig', [
            'sondage' => $sondage,
            'questions' => $sondage->getQuestions()
        ]);
    }

    #[IsGranted("ROLE_SONDEUR")]
    #[Route('/{id}/changerPublication', name: 'app_controleur_sondage_changerPublication', methods: ['GET'])]
    public function changerPublication(Request $request, Sondage $sondage, SondageRepository $sondageRepository): Response {
        $etat = $sondage->isEstPublie();
        $sondage->setEstPublie(!$etat);
        $this->manager->persist($sondage);
        $this->manager->flush($sondage);
        return $this->redirect($request->headers->get('referer'));
//        return $this->redirectToRoute("app_controleur_sondage_show", ["id" => $sondage->getId()]);
    }

    #[IsGranted("ROLE_SONDEUR")]
    #[Route('/{id}/edit', name: 'app_controleur_sondage_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Sondage $sondage, SondageRepository $sondageRepository, QuestionRepository $questionRepository): Response
    {
        $form = $this->createForm(SondageType::class, $sondage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sondageRepository->save($sondage, true);

            return $this->redirectToRoute('app_controleur_sondage_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('controleur_sondage/edit.html.twig', [
            'sondage' => $sondage,
            'form' => $form,
            'questions' => $sondage->getQuestions()
        ]);
    }
    #[IsGranted("ROLE_SONDEUR")]
    #[Route('/{id}', name: 'app_controleur_sondage_delete', methods: ['POST'])]
    public function delete(Request $request, Sondage $sondage, SondageRepository $sondageRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sondage->getId(), $request->request->get('_token'))) {
            $sondageRepository->remove($sondage, true);
        }
        return $this->redirectToRoute('app_controller_produit_show', ["id" => $sondage->getProduit()->getId()], Response::HTTP_SEE_OTHER);
    }


}
