<?php

namespace App\Controller;

use App\Entity\Question;
use App\Entity\Statistiques;
use App\Form\StatistiquesType;
use App\Repository\StatistiquesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

$GLOBALS['dispo'] = [
    'choix_possible' => [
        array(
            0 => "Nombre de réponse",
            1 => "Sexe sur représenté",
            2 => "Département sur représenté",
            3 => "Réponse la plus répondu",
            4 => "Réponse la moins répondu",
            5 => "Mot qui revient le plus",
            6 => "Mot qui revient le moins",
            7 => "Le plus ancien",
            8 => "Le plus récent",
            9 => "Moyenne",
            10 => "Max",
            11 => "Min",
            12 => "Note moyenne",
            13 => "Note max",
            14 => "Note min",
            15 => "Répartition des sexes",
            16 => "Répartition des résultats",
            17 => "Répartition des départements"
        )
    ],
    'type_possible' => [
        array(
            0 => "Valeur simple",
            1 => "Histogramme",
            2 => "Graphique"
        )
    ]
];


#[Route('/statistiques')]
class ControllerStatistiques extends AbstractController
{
    #[IsGranted("ROLE_SONDEUR")]
    #[Route('/new/{id}/{id_type_choix}', name: 'app_statistiques_new', methods: ['GET', 'POST'])]
    public function new(Request $request, StatistiquesRepository $statistiquesRepository, Question $question, int $id_type_choix): Response
    {

        if ($request->get("valeur") != null and array_key_exists($request->get("valeur"), $GLOBALS['dispo']['choix_possible'][0])) {
            $statistique = new Statistiques();
            $statistique->setUtilisateur($this->getUser());
            $statistique->setQuestion($question);
            $statistique->setType($id_type_choix);
            $statistique->setRequete($request->get("valeur"));
            $statistiquesRepository->save($statistique, true);
            return $this->redirectToRoute('app_statistiques_show', ["id" => $question->getId()], Response::HTTP_SEE_OTHER);
        }
        $redirect = "app_statistiques_index";
        if (array_key_exists($id_type_choix, $GLOBALS['dispo']['type_possible'][0])) {
            switch ($id_type_choix) {
                case 0 :
                {
                    $redirect = "new_valeur_simple";
                    break;
                }
                case 1 :
                {
                    $redirect = "new_histogramme";
                    break;
                }
                case 2 :
                {
                    $redirect = "new_graphique";
                    break;
                }
            }
        } else {
            dd("La valeur n'exsite pas");
        }

        return $this->renderForm('controleur_statistiques/' . $redirect . '.html.twig', [
            'question' => $question,
            'type_choix' => $id_type_choix
        ]);
    }


    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SONDEUR')")
     */
    #[Route('/{id}', name: 'app_statistiques_show', methods: ['GET', 'POST'])]
    public function new_type(Request $request, Question $question): Response
    {
        if ($request->get("choix") != null) {
            return $this->redirectToRoute('app_statistiques_new', ["id" => $question->getId(), "id_type_choix" => $request->get("choix")], Response::HTTP_SEE_OTHER);
        }
        $listeStatistiquesUser = $question->getStatistiques();
        $resStats = [];
        $user = $this->getUser();
        foreach ($listeStatistiquesUser as $stats){
            if ($stats->getUtilisateur() === $user){
                $resStats[] = $stats;
            }
        }
        $valeurStatistiques = [];
        foreach ($resStats as $stats){
            $stat = [];
            $stat["stats"] = $stats;
            switch ($stats->getType()){
                case 0 : {
                    switch ($stats->getRequete()){
                        case 0 : {
                            $stat["donnees"] = $stats->getNombreDeReponseTotal();
                            break;
                        }
                        case 1  :
                        {
                            $stat["donnees"] = $stats->getSexeSurRepresente();
                            break;
                        }
                        case 2  :
                        {
                            $stat["donnees"] = $stats->getDepartementSurRepresente();
                            break;
                        }
                        case 3  :
                        {
                            $stat["donnees"] = $stats->getReponseLaPlusRepondu();
                            break;
                        }
                        case 4  :
                        {
                            $stat["donnees"] = $stats->getReponseLaMoinsRepondu();
                            break;
                        }
                        case 5  :
                        {
                            $stat["donnees"] = $stats->getMotQuiRevientLePlus();
                            break;
                        }

                        case 6  :
                        {
                            $stat["donnees"] = $stats->getMotQuiRevientLeMoins();
                            break;
                        }
                        case 7  :
                        {
                            $stat["donnees"] = $stats->getDateLaPlusAncienne();
                            break;
                        }
                        case 8  :
                        {
                            $stat["donnees"] = $stats->getDateLaPlusRecente();
                            break;
                        }
                        case 12 :
                        case 9  :
                        {
                            $stat["donnees"] = $stats->getMoyenneNombre();
                            break;
                        }
                        case 13 :
                        case 10 :
                        {
                            $stat["donnees"] = $stats->getMaxNombre();
                            break;
                        }
                        case 14 :
                        case 11 :
                        {
                            $stat["donnees"] = $stats->getMinNombre();
                            break;
                        }
                    }
                    break;
                }
                case 1 : {
                    switch ($stats->getRequete()){
                        case 15 : {
                            $stat["donnees"] = $stats->getRepartitionDesSexes();
                            break;
                        }
                        case 16 : {
                            if ($stats->getQuestion()->getTypeQuestion() == "input"){
                                $stat["donnees"] = $stats->getTop10Mots();
                            } else {
                                $stat["donnees"] = $stats->getRepartitionDesReponses();
                            }

                            break;
                        }
                        case 17 : {
                            $stat["donnees"] = $stats->getRepartitionDesCodesPostaux();
                            break;
                        }

                    }
                    break;
                }
            }
            $valeurStatistiques[] = $stat;
        }
        return $this->renderForm('controleur_statistiques/show.html.twig', [
            'question' => $question,
            'statistiques' => $valeurStatistiques
        ]);
    }
    #[IsGranted("ROLE_SONDEUR")]
    #[Route('/{id}/edit', name: 'app_statistiques_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Statistiques $statistique, StatistiquesRepository $statistiquesRepository): Response
    {
        $form = $this->createForm(StatistiquesType::class, $statistique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $statistiquesRepository->save($statistique, true);

            return $this->redirectToRoute('app_statistiques_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('controleur_statistiques/edit.html.twig', [
            'statistique' => $statistique,
            'form' => $form,
        ]);
    }

    #[IsGranted("ROLE_SONDEUR")]
    #[Route('/{id}', name: 'app_statistiques_delete', methods: ['POST'])]
    public function delete(Request $request, Statistiques $statistique, StatistiquesRepository $statistiquesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$statistique->getId(), $request->request->get('_token'))) {
            $statistiquesRepository->remove($statistique, true);
        }

        return $this->redirectToRoute('app_statistiques_index', [], Response::HTTP_SEE_OTHER);
    }
}
