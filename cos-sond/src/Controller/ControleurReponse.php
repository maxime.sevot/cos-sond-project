<?php

namespace App\Controller;

use App\Entity\Question;
use App\Entity\Reponse;
use App\Form\ReponseType;
use App\Repository\ReponseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

#[Route('/reponse')]
class ControleurReponse extends AbstractController
{
    #[IsGranted("ROLE_ADMIN")]
    #[Route('/', name: 'app_controleur_reponse_index', methods: ['GET'])]
    public function index(ReponseRepository $reponseRepository): Response
    {
        return $this->render('controleur_reponse/index.html.twig', [
            'reponses' => $reponseRepository->findAll(),
        ]);
    }

    #[IsGranted("ROLE_SONDEUR")]
    #[Route('/new/{id}', name: 'app_controleur_reponse_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ReponseRepository $reponseRepository, Question $question): Response
    {
        $reponse = new Reponse();
        $reponse->setQuestion($question);
        $form = $this->createForm(ReponseType::class, $reponse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reponseRepository->save($reponse, true);

            return $this->redirectToRoute('app_controleur_sondage_show', ["id" => $question->getSondage()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('controleur_reponse/new.html.twig', [
            'reponse' => $reponse,
            'form' => $form,
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SONDEUR')")
     */
    #[Route('/{id}', name: 'app_controleur_reponse_show', methods: ['GET'])]
    public function show(Reponse $reponse): Response
    {
        return $this->render('controleur_reponse/show.html.twig', [
            'reponse' => $reponse,
        ]);
    }

    #[IsGranted("ROLE_SONDEUR")]
    #[Route('/{id}/edit', name: 'app_controleur_reponse_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Reponse $reponse, ReponseRepository $reponseRepository): Response
    {
        $form = $this->createForm(ReponseType::class, $reponse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reponseRepository->save($reponse, true);

            return $this->redirectToRoute('app_controleur_reponse_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('controleur_reponse/edit.html.twig', [
            'reponse' => $reponse,
            'form' => $form,
        ]);
    }

    #[IsGranted("ROLE_SONDEUR")]
    #[Route('/{id}', name: 'app_controleur_reponse_delete', methods: ['POST'])]
    public function delete(Request $request, Reponse $reponse, ReponseRepository $reponseRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$reponse->getId(), $request->request->get('_token'))) {
            $reponseRepository->remove($reponse, true);
        }

        return $this->redirectToRoute('app_controleur_reponse_index', [], Response::HTTP_SEE_OTHER);
    }
}
