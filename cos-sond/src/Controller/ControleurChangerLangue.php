<?php

namespace App\Controller;

use App\Event\ChangeLanguageEvent;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ControleurChangerLangue extends AbstractController{

    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    #[Route("/language/change/{locale}", name: 'change_locale')]
    public function changeLocale(Request $request, SessionInterface $session, $locale, EventDispatcherInterface $eventDispatcher): Response
    {
        $session->set('_locale', $locale);
        $request->setLocale($locale);
        $this->translator->setLocale($locale);
        $referer = $request->headers->get('referer');
        $eventDispatcher->dispatch(new ChangeLanguageEvent($locale));
        return $this->redirect($referer);
    }
}
