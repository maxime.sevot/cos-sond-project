<?php

namespace App\Controller;

use App\Entity\Question;
use App\Entity\Reponse;
use App\Entity\Sondage;
use App\Form\QuestionType;
use App\Repository\QuestionRepository;
use App\Repository\ReponseRepository;
use PHPUnit\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

#[Route('/question')]
class ControleurQuestion extends AbstractController
{
    #[IsGranted("ROLE_ADMIN")]
    #[Route('/', name: 'app_controleur_question_index', methods: ['GET'])]
    public function index(QuestionRepository $questionRepository): Response
    {
        return $this->render('controleur_question/index.html.twig', [
            'questions' => $questionRepository->findAll(),
        ]);
    }

    #[IsGranted("ROLE_SONDEUR")]
    #[Route('/new/{id}', name: 'app_controleur_question_new', methods: ['GET', 'POST'])]
    public function new(Sondage $sondage, Request $request, QuestionRepository $questionRepository, ReponseRepository $reponseRepository): Response
    {
        $question = new Question();
        $question->setSondage($sondage);
        $form = $this->createForm(QuestionType::class, $question);
        $form->handleRequest($request);

        $numero_question = (int) $sondage->getNombreQuestions() + 1;
        $question->setNumeroQuestion($numero_question);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('typeQuestion')->getData() != "qcm" && $form->get('typeQuestion')->getData() != "qcs"){
                $reponse = new Reponse();
                $reponse->setQuestion($question);
                $reponse->setIntitule($form->get('typeQuestion')->getData());
                $reponseRepository->save($reponse);
            }
            $questionRepository->save($question, true);

            return $this->redirectToRoute('app_controleur_sondage_show', ['id' => $sondage->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('controleur_question/new.html.twig', [
            'question' => $question,
            'form' => $form,
            'numero_question' => $numero_question
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SONDEUR')")
     */
    #[Route('/{id}', name: 'app_controleur_question_show', methods: ['GET'])]
    public function show(Question $question): Response
    {
        return $this->render('controleur_question/show.html.twig', [
            'question' => $question,
        ]);
    }

    #[IsGranted("ROLE_SONDEUR")]

    #[Route('/{id}/edit', name: 'app_controleur_question_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Question $question, QuestionRepository $questionRepository): Response
    {
        $form = $this->createForm(QuestionType::class, $question);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $questionRepository->save($question, true);

            return $this->redirectToRoute('app_controleur_sondage_show', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('controleur_question/edit.html.twig', [
            'question' => $question,
            'form' => $form,
        ]);
    }

    #[IsGranted("ROLE_SONDEUR")]
    #[Route('/{id}', name: 'app_controleur_question_delete', methods: ['POST'])]
    public function delete(Request $request, Question $question, QuestionRepository $questionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$question->getId(), $request->request->get('_token'))) {
            $questionRepository->remove($question, true);
        }
        return $this->redirectToRoute('app_controleur_sondage_show', ["id" => $question->getSondage()->getId()], Response::HTTP_SEE_OTHER);
    }
}
