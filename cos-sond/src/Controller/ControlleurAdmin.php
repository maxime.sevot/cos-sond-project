<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class ControlleurAdmin extends AbstractController
{

    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    #[IsGranted("ROLE_ADMIN")]
    #[Route('/admin', name: 'app_admin')]
    public function index(Request $request, UserRepository $userRepository, PaginatorInterface $paginator): Response
    {

        $users = $userRepository->findBy(array(), array('nom' => 'ASC'));
        $userpage = $paginator->paginate($users, $request->query->getInt('page',1, 10));
        return $this->render('controleur_admin/index.html.twig', [
            'users' => $userpage,
        ]);
    }

    #[IsGranted("ROLE_ADMIN")]
    #[Route('/mettreroles/{id}/{role}', name: 'app_mettre_admin')]
    public function mettreroles(User $user, String $role): Response
    {
        $user->addRole($role);
        $this->manager->persist($user);
        $this->manager->flush();
        return $this->redirectToRoute("app_admin");
    }

    #[IsGranted("ROLE_ADMIN")]
    #[Route('/enleverroles/{id}/{role}', name: 'app_enlever_admin')]
    public function enleverroles(User $user, String $role): Response
    {
        $user->removeRole($role);
        $this->manager->persist($user);
        $this->manager->flush();
        return $this->redirectToRoute("app_admin");
    }
}
