<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ControllerHome extends AbstractController
{
    #[Route('/', name: 'app_controller_home')]
    public function index(ProduitRepository $produitRepository): Response
    {
        return $this->render('controller_home/index.html.twig', [
            'controller_name' => 'ControllerHome',
            "produits" => $produitRepository->findAll()
        ]);
    }

    #[Route('/apropos', name: 'app_controller_about')]
    public function about(ProduitRepository $produitRepository): Response
    {
        return $this->render('controller_home/about.html.twig', [
        ]);
    }
}
