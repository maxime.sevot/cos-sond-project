<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Form\ProduitType;
use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

#[Route('/produit')]
class ControllerProduit extends AbstractController
{
    #[IsGranted("ROLE_ADMIN")]
    #[Route('/', name: 'app_controller_produit_index', methods: ['GET'])]
    public function index(ProduitRepository $produitRepository): Response
    {
        return $this->render('controller_produit/index.html.twig', [
            'produits' => $produitRepository->findAll(),
        ]);
    }

    #[IsGranted("ROLE_SONDEUR")]
    #[Route('/new', name: 'app_controller_produit_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ProduitRepository $produitRepository): Response
    {
        $produit = new Produit();
        $form = $this->createForm(ProduitType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form['image']->getData();
            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()).'.'.$image->guessExtension();
            $produit->setImage($fileName);

            // Move the file to the directory where images are stored
            try {
                $image->move(
                    $this->getParameter('images_directory'),
                    $fileName
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }
            $produitRepository->save($produit, true);

            return $this->redirectToRoute('app_controller_produit_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('controller_produit/new.html.twig', [
            'produit' => $produit,
            'form' => $form,
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SONDEUR')")
     */
    #[Route('/{id}', name: 'app_controller_produit_show', methods: ['GET'])]
    public function show(Produit $produit): Response
    {
//        return $this->render('controller_sondage/index.html.twig', [
        return $this->render('controller_produit/show.html.twig', [
            'produit' => $produit,
        ]);
    }

    #[IsGranted("ROLE_SONDEUR")]
    #[Route('/{id}/edit', name: 'app_controller_produit_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Produit $produit, ProduitRepository $produitRepository): Response
    {
        $form = $this->createForm(ProduitType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $produitRepository->save($produit, true);

            return $this->redirectToRoute('app_controller_produit_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('controller_produit/edit.html.twig', [
            'produit' => $produit,
            'form' => $form,
        ]);
    }

    #[IsGranted("ROLE_SONDEUR")]

    #[Route('/{id}', name: 'app_controller_produit_delete', methods: ['POST'])]
    public function delete(Request $request, Produit $produit, ProduitRepository $produitRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$produit->getId(), $request->request->get('_token'))) {
            $produitRepository->remove($produit, true);
        }

        return $this->redirectToRoute('app_controller_produit_index', [], Response::HTTP_SEE_OTHER);
    }
}
