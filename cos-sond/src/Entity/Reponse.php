<?php

namespace App\Entity;

use App\Repository\ReponseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReponseRepository::class)]
class Reponse
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $intitule = null;

    #[ORM\ManyToOne(inversedBy: 'reponses')]
    private ?Question $question = null;

    #[ORM\OneToMany(mappedBy: 'reponse', targetEntity: QuestionRepondue::class, cascade: ["remove"])]
    private Collection $questionRepondues;

    public function __construct()
    {
        $this->questionRepondues = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    public function setQuestion(?Question $question): self
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return Collection<int, QuestionRepondue>
     */
    public function getQuestionRepondues(): Collection
    {
        return $this->questionRepondues;
    }

    public function addQuestionRepondue(QuestionRepondue $questionRepondue): self
    {
        if (!$this->questionRepondues->contains($questionRepondue)) {
            $this->questionRepondues->add($questionRepondue);
            $questionRepondue->setReponse($this);
        }

        return $this;
    }

    public function removeQuestionRepondue(QuestionRepondue $questionRepondue): self
    {
        if ($this->questionRepondues->removeElement($questionRepondue)) {
            // set the owning side to null (unless already changed)
            if ($questionRepondue->getReponse() === $this) {
                $questionRepondue->setReponse(null);
            }
        }

        return $this;
    }
}
