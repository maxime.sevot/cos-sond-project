<?php

namespace App\Entity;

use App\Repository\SondageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SondageRepository::class)]
class Sondage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $theme = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $dateCreation = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $dateFin = null;

    #[ORM\ManyToOne(inversedBy: 'sondages')]
    private ?User $createur = null;

    #[ORM\OneToMany(mappedBy: 'sondage', targetEntity: Question::class, cascade: ["remove"])]
    private Collection $questions;

    #[ORM\ManyToOne(inversedBy: 'sondages')]
    private ?Produit $produit = null;

    #[ORM\Column]
    private ?bool $estPublie = null;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTheme(): ?string
    {
        return $this->theme;
    }

    public function setTheme(string $theme): self
    {
        $this->theme = $theme;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getCreateur(): ?User
    {
        return $this->createur;
    }

    public function setCreateur(?User $createur): self
    {
        $this->createur = $createur;

        return $this;
    }

    /**
     * @return Collection<int, Question>
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions->add($question);
            $question->setSondage($this);
        }

        return $this;
    }

    public function removeQuestion(Question $question): self
    {
        if ($this->questions->removeElement($question)) {
            // set the owning side to null (unless already changed)
            if ($question->getSondage() === $this) {
                $question->setSondage(null);
            }
        }

        return $this;
    }

    public function getQuestionsWithResponses(ReponseRepository $reponseRepository, QuestionRepository $questionRepository)
    {
        $questions = $questionRepository->findBy(['id_sondage' => $this->id]);

        foreach ($questions as $question) {
            if ($question->getTypeQuestion() === 'qcm' || $question->getTypeQuestion() === 'qcs') {
                $reponses = $reponseRepository->findBy(['question' => $question->getId()]);
                $question->setReponses($reponses);
            }
        }

        return $questions;
    }

//    public function getUnansweredSondages(SondageRepository $sondageRepository, User $user): array
//    {
//        // Récupération de toutes les QuestionRepondues de l'utilisateur
//        $questionRepondues = $user->getQuestionRepondues();
//
//        $answeredSondageIds = [];
//
//        // Pour chaque QuestionRepondue, on récupère le Sondage correspondant
//        foreach ($questionRepondues as $questionRepondue) {
//            $question = $questionRepondue->getQuestionFromQuestionRepondue();
//            if ($question !== null) {
//                $sondage = $question->getSondage();
//                if ($sondage !== null) {
//                    $answeredSondageIds[] = $sondage->getId();
//                }
//            }
//        }
//
//        // Récupération de tous les Sondages
//        $allSondages = $sondageRepository->findAll();
//        $allSondageIds = array_map(fn(Sondage $sondage) => $sondage->getId(), $allSondages);
//
//        // Soustraction des Sondages auxquels l'utilisateur a répondu pour obtenir ceux auxquels il n'a pas répondu
//        $unansweredSondageIds = array_diff($allSondageIds, $answeredSondageIds);
//
//        // Récupération des Sondages auxquels l'utilisateur n'a pas répondu
//        $unansweredSondages = [];
//        foreach ($unansweredSondageIds as $sondageId) {
//            $sondage = $sondageRepository->find($sondageId);
//            if ($sondage !== null) {
//                $unansweredSondages[] = $sondage;
//            }
//        }
//
//        return $unansweredSondages;
//    }

    public function getReponses(): Collection
    {
        $reponses = new ArrayCollection();

        foreach ($this->questions as $question) {
            foreach ($question->getReponses() as $reponse) {
                $reponses->add($reponse);
            }
        }

        return $reponses;
    }

    public function getNombreQuestions(){
        return $this->questions->count();
    }

    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    public function setProduit(?Produit $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    public function isEstPublie(): ?bool
    {
        return $this->estPublie;
    }

    public function setEstPublie(bool $estPublie): self
    {
        $this->estPublie = $estPublie;

        return $this;
    }

//    public function getUnansweredSondagesByProduct(SondageRepository $sondageRepository, User $user, Produit $produit) : array
//    {
//        $listeSondagesProduit = $produit->getSondages()->toArray();
//        $sondageNonRepondu = $this->getUnansweredSondages($sondageRepository, $user);
//        $liste_des_sondages_non_repondu_dun_produit_pour_un_utilisateur = [];
//        foreach ($listeSondagesProduit as $sondageProduit){
//            foreach ($sondageNonRepondu as $sondageNR){
//                if ($sondageProduit == $sondageNR){
//                    $liste_des_sondages_non_repondu_dun_produit_pour_un_utilisateur[] = $sondageProduit;
//                }
//            }
//        }
//        return $liste_des_sondages_non_repondu_dun_produit_pour_un_utilisateur;
//    }
}
