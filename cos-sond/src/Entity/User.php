<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    private ?string $prenom = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $dateNaissance = null;

    #[ORM\Column]
    private ?int $genre = null;

    #[ORM\Column]
    private ?int $codePostal = null;

    #[ORM\OneToMany(mappedBy: 'createur', targetEntity: Sondage::class, cascade: ["remove"])]
    private Collection $sondages;

    #[ORM\OneToMany(mappedBy: 'utilisateur', targetEntity: QuestionRepondue::class, cascade: ["remove"])]
    private Collection $questionRepondues;

    #[ORM\OneToMany(mappedBy: 'utilisateur', targetEntity: Statistiques::class, cascade: ["remove"])]
    private Collection $statistiques;

    public function __construct()
    {
        $this->sondages = new ArrayCollection();
        $this->questionRepondues = new ArrayCollection();
        $this->statistiques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
//        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function addRole(String $role): self
    {
        $this->roles[] = $role;
        $this->setRoles($this->roles);
        return $this;
    }

    public function removeRole(String $role): self
    {
        $index = array_search($role, $this->roles);
        if ($index !== false) {
            array_splice($this->roles, $index, 1);
        }
        $this->setRoles($this->roles);
        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getGenre(): ?int
    {
        return $this->genre;
    }

    public function setGenre(int $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getCodePostal(): ?int
    {
        return $this->codePostal;
    }

    public function setCodePostal(int $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * @return Collection<int, Sondage>
     */
    public function getSondages(): Collection
    {
        return $this->sondages;
    }

    public function addSondage(Sondage $sondage): self
    {
        if (!$this->sondages->contains($sondage)) {
            $this->sondages->add($sondage);
            $sondage->setCreateur($this);
        }

        return $this;
    }

    public function removeSondage(Sondage $sondage): self
    {
        if ($this->sondages->removeElement($sondage)) {
            // set the owning side to null (unless already changed)
            if ($sondage->getCreateur() === $this) {
                $sondage->setCreateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QuestionRepondue>
     */
    public function getQuestionRepondues(): Collection
    {
        return $this->questionRepondues;
    }

    public function addQuestionRepondue(QuestionRepondue $questionRepondue): self
    {
        if (!$this->questionRepondues->contains($questionRepondue)) {
            $this->questionRepondues->add($questionRepondue);
            $questionRepondue->setUtilisateur($this);
        }

        return $this;
    }

    public function removeQuestionRepondue(QuestionRepondue $questionRepondue): self
    {
        if ($this->questionRepondues->removeElement($questionRepondue)) {
            // set the owning side to null (unless already changed)
            if ($questionRepondue->getUtilisateur() === $this) {
                $questionRepondue->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Statistiques>
     */
    public function getStatistiques(): Collection
    {
        return $this->statistiques;
    }

    public function addStatistique(Statistiques $statistique): self
    {
        if (!$this->statistiques->contains($statistique)) {
            $this->statistiques->add($statistique);
            $statistique->setUtilisateur($this);
        }

        return $this;
    }

    public function removeStatistique(Statistiques $statistique): self
    {
        if ($this->statistiques->removeElement($statistique)) {
            // set the owning side to null (unless already changed)
            if ($statistique->getUtilisateur() === $this) {
                $statistique->setUtilisateur(null);
            }
        }

        return $this;
    }
}
