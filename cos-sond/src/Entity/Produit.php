<?php

namespace App\Entity;

use App\Repository\ProduitRepository;
use App\Repository\SondageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProduitRepository::class)]
class Produit
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $image = null;

    #[ORM\OneToMany(mappedBy: 'produit', targetEntity: Sondage::class, cascade: ["remove"])]
    private Collection $sondages;

    public function __construct()
    {
        $this->sondages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection<int, Sondage>
     */
    public function getSondages(): Collection
    {
        return $this->sondages;
    }

    public function addSondage(Sondage $sondage): self
    {
        if (!$this->sondages->contains($sondage)) {
            $this->sondages->add($sondage);
            $sondage->setProduit($this);
        }

        return $this;
    }

    public function removeSondage(Sondage $sondage): self
    {
        if ($this->sondages->removeElement($sondage)) {
            // set the owning side to null (unless already changed)
            if ($sondage->getProduit() === $this) {
                $sondage->setProduit(null);
            }
        }

        return $this;
    }

    public function getUnansweredSondages(User $user): array
    {
        // Récupération de toutes les QuestionRepondues de l'utilisateur
        $questionRepondues = $user->getQuestionRepondues();

        $answeredSondageIds = [];

        // Pour chaque QuestionRepondue, on récupère le Sondage correspondant
        foreach ($questionRepondues as $questionRepondue) {
            $question = $questionRepondue->getQuestionFromQuestionRepondue();
            if ($question !== null) {
                $sondage = $question->getSondage();
                if ($sondage !== null) {
                    $answeredSondageIds[] = $sondage->getId();
                }
            }
        }

        // Récupération de tous les Sondages
        $allSondages = $this->sondages;
        $allSondageIds = array_map(fn(Sondage $sondage) => $sondage->getId(), $allSondages->toArray());

        // Soustraction des Sondages auxquels l'utilisateur a répondu pour obtenir ceux auxquels il n'a pas répondu
        $unansweredSondageIds = array_diff($allSondageIds, $answeredSondageIds);

        // Récupération des Sondages auxquels l'utilisateur n'a pas répondu
        $unansweredSondages = [];
        foreach ($unansweredSondageIds as $sondageId) {
            $sondage = $allSondages->filter(function($sond) use ($sondageId) {
                return $sond->getId() === $sondageId;
            })->first();
            if ($sondage !== null) {
                $unansweredSondages[] = $sondage;
            }
        }
        $unansweredSondagesVisible = [];
        foreach ($unansweredSondages as $sond){
            if ($sond->isEstPublie()){
                $unansweredSondagesVisible[] = $sond;
            }
        }

        return $unansweredSondagesVisible;
    }

    public function getUnansweredSondagesByProduct(User $user) : array
    {
        $listeSondagesProduit = $this->getSondages()->toArray();
        $sondageNonRepondu = $this->getUnansweredSondages($user);
        $liste_des_sondages_non_repondu_dun_produit_pour_un_utilisateur = [];
        foreach ($listeSondagesProduit as $sondageProduit){

            foreach ($sondageNonRepondu as $sondageNR){
                if ($sondageProduit == $sondageNR){
                    $liste_des_sondages_non_repondu_dun_produit_pour_un_utilisateur[] = $sondageProduit;
                }
            }
        }
        return $liste_des_sondages_non_repondu_dun_produit_pour_un_utilisateur;
    }
}
