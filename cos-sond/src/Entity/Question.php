<?php

namespace App\Entity;

use App\Repository\QuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: QuestionRepository::class)]
class Question
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?bool $estOptionnelle = null;

    #[ORM\Column(length: 255)]
    private ?string $typeQuestion = null;

    #[ORM\Column(length: 255)]
    private ?string $titreQuestion = null;

    #[ORM\Column]
    private ?int $numeroQuestion = null;

    #[ORM\ManyToOne(inversedBy: 'questions')]
    private ?Sondage $sondage = null;

    #[ORM\OneToMany(mappedBy: 'question', targetEntity: Reponse::class, cascade: ["remove"])]
    private Collection $reponses;

    #[ORM\OneToMany(mappedBy: 'question', targetEntity: Statistiques::class, cascade: ["remove"])]
    private Collection $statistiques;

    public function __construct()
    {
        $this->reponses = new ArrayCollection();
        $this->statistiques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isEstOptionnelle(): ?bool
    {
        return $this->estOptionnelle;
    }

    public function setEstOptionnelle(bool $estOptionnelle): self
    {
        $this->estOptionnelle = $estOptionnelle;

        return $this;
    }

    public function getTypeQuestion(): ?string
    {
        return $this->typeQuestion;
    }

    public function setTypeQuestion(string $typeQuestion): self
    {
        $this->typeQuestion = $typeQuestion;

        return $this;
    }

    public function getTitreQuestion(): ?string
    {
        return $this->titreQuestion;
    }

    public function setTitreQuestion(string $titreQuestion): self
    {
        $this->titreQuestion = $titreQuestion;

        return $this;
    }

    public function getNumeroQuestion(): ?int
    {
        return $this->numeroQuestion;
    }

    public function setNumeroQuestion(int $numeroQuestion): self
    {
        $this->numeroQuestion = $numeroQuestion;

        return $this;
    }

    public function getSondage(): ?Sondage
    {
        return $this->sondage;
    }

    public function setSondage(?Sondage $sondage): self
    {
        $this->sondage = $sondage;

        return $this;
    }

    /**
     * @return Collection<int, Reponse>
     */
    public function getReponses(): Collection
    {
        return $this->reponses;
    }

    public function addReponse(Reponse $reponse): self
    {
        if (!$this->reponses->contains($reponse)) {
            $this->reponses->add($reponse);
            $reponse->setQuestion($this);
        }

        return $this;
    }

    public function removeReponse(Reponse $reponse): self
    {
        if ($this->reponses->removeElement($reponse)) {
            // set the owning side to null (unless already changed)
            if ($reponse->getQuestion() === $this) {
                $reponse->setQuestion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Statistiques>
     */
    public function getStatistiques(): Collection
    {
        return $this->statistiques;
    }

    public function addStatistique(Statistiques $statistique): self
    {
        if (!$this->statistiques->contains($statistique)) {
            $this->statistiques->add($statistique);
            $statistique->setQuestion($this);
        }

        return $this;
    }

    public function removeStatistique(Statistiques $statistique): self
    {
        if ($this->statistiques->removeElement($statistique)) {
            // set the owning side to null (unless already changed)
            if ($statistique->getQuestion() === $this) {
                $statistique->setQuestion(null);
            }
        }

        return $this;
    }

//    public function getQuestionRepondues(): Collection
//    {
//        return $this->reponses->map(function (Reponse $reponse) {
//            return $reponse->getQuestionRepondues();
//        });
//    }

    public function getQuestionRepondues(): array
    {
        $questionRepondues = [];
        $this->reponses->map(function (Reponse $reponse) use (&$questionRepondues) {
            $questionRepondues = array_merge($questionRepondues, $reponse->getQuestionRepondues()->toArray());
        });
        return $questionRepondues;
    }


    public function getAgeMoyenDesReponses(): float
    {
        $ageSum = 0;
        $userCount = 0;

        foreach ($this->reponses as $reponse) {
            foreach ($reponse->getQuestionRepondues() as $questionRepondue) {
                $user = $questionRepondue->getUtilisateur();
                if ($user) {
                    $age = $user->getDateNaissance()->diff(new \DateTime())->y;
                    $ageSum += $age;
                    $userCount++;
                }
            }
        }

        if ($userCount > 0) {
            return $ageSum / $userCount;
        }

        return 0;
    }

    public function getAgeMaxDesReponses(): int
    {
        $maxAge = 0;
        foreach ($this->reponses as $reponse) {
            foreach ($reponse->getQuestionRepondues() as $questionRepondue) {

                    $user = $questionRepondue->getUtilisateur();
                    if ($user) {
                        $age = $user->getAge();
                        if ($age > $maxAge) {
                            $maxAge = $age;
                        }
                    }
                }
        }
        return $maxAge;
    }


}
