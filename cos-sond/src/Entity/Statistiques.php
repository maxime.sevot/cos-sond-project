<?php

namespace App\Entity;

use App\Repository\ReponseRepository;
use App\Repository\StatistiquesRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Collection;

#[ORM\Entity(repositoryClass: StatistiquesRepository::class)]
class Statistiques
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $requete = null;

    #[ORM\ManyToOne(inversedBy: 'controleur_statistiques')]
    private ?User $utilisateur = null;

    #[ORM\ManyToOne(inversedBy: 'controleur_statistiques')]
    private ?Question $question = null;

    #[ORM\Column]
    private ?int $type = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRequete(): ?string
    {
        return $this->requete;
    }

    public function setRequete(string $requete): self
    {
        $this->requete = $requete;

        return $this;
    }

    public function getUtilisateur(): ?User
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?User $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    public function setQuestion(?Question $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUtilisateurQuiOntRepondus() : array
    {
        $res = [];
        foreach ($this->question->getReponses() as $reponse){
            foreach ($reponse->getQuestionRepondues() as $questionRepondue){
                $user = $questionRepondue->getUtilisateur();
                if (!in_array($user, $res)){
                    $res[] = $user;
                }
            }
        }
        return $res;
    }

    // METHODES POUR LES STATISTIQUES A VALEURS SIMPLE

    public function getNombreDeReponseTotal(): int{
        return count($this->question->getQuestionRepondues());
    }

    public function getDepartementSurRepresente(): string {
        $postalCodeCounts = [];

        foreach ($this->getUtilisateurQuiOntRepondus() as $user){
            $postalCode = $user->getCodePostal();
            if (!isset($postalCodeCounts[$postalCode])) {
                $postalCodeCounts[$postalCode] = 0;
            }
            $postalCodeCounts[$postalCode]++;
        }

        $mostFrequentPostalCode = null;
        $maxCount = 0;
        foreach ($postalCodeCounts as $postalCode => $count) {
            if ($count > $maxCount) {
                $mostFrequentPostalCode = $postalCode;
                $maxCount = $count;
            }
        }

        return $mostFrequentPostalCode;
    }

    public function getSexeSurRepresente(): string {
        $sexeCount = [];

        foreach ($this->getUtilisateurQuiOntRepondus() as $user){
            $sexe = $user->getGenre();
            if (!isset($sexeCount[$sexe])) {
                $sexeCount[$sexe] = 0;
            }
            $sexeCount[$sexe]++;
        }

        $mostFrequentSexe = null;
        $maxCount = 0;
        foreach ($sexeCount as $sexe => $count) {
            if ($count > $maxCount) {
                $mostFrequentSexe = $sexe;
                $maxCount = $count;
            }
        }

        if ($mostFrequentSexe == 0){
            return "Homme";
        } elseif($mostFrequentSexe == 1) {
            return "Femme";
        } else {
            return "Autres";
        }
    }

    public function getReponseLaPlusRepondu(): string {
        $repartitionReponses = [];

        foreach ($this->question->getQuestionRepondues() as $questionRepondue){
            if (!isset($repartitionReponses[$questionRepondue->getReponse()->getId()])) {
                $repartitionReponses[$questionRepondue->getReponse()->getId()] = 0;
            }
            $repartitionReponses[$questionRepondue->getReponse()->getId()]++;
        }

        $mostFrequentReponse = null;
        $maxCount = 0;
        foreach ($repartitionReponses as $rep => $count) {
            if ($count > $maxCount) {
                $mostFrequentReponse = $rep;
                $maxCount = $count;
            }
        }
        $res = $this->question->getReponses()->filter(function(Reponse $reponse) use ($mostFrequentReponse){
            return $reponse->getId() === $mostFrequentReponse;
        })->first();
        return $res->getIntitule();
    }

    public function getReponseLaMoinsRepondu(): string {
        $repartitionReponses = [];

        foreach ($this->question->getQuestionRepondues() as $questionRepondue){
            if (!isset($repartitionReponses[$questionRepondue->getReponse()->getId()])) {
                $repartitionReponses[$questionRepondue->getReponse()->getId()] = 0;
            }
            $repartitionReponses[$questionRepondue->getReponse()->getId()]++;
        }

        $mostFrequentReponse = null;
        $minCount = null;
        foreach ($repartitionReponses as $rep => $count) {
            if ($minCount == null || $count < $minCount) {
                $mostFrequentReponse = $rep;
                $minCount = $count;
            }
        }
        $res = $this->question->getReponses()->filter(function(Reponse $reponse) use ($mostFrequentReponse){
            return $reponse->getId() === $mostFrequentReponse;
        })->first();
        return $res->getIntitule();
    }

    public function getMotQuiRevientLePlus(): string  {
        $questionRepondues = $this->getQuestion()->getQuestionRepondues();
        $wordCounts = [];
        foreach ($questionRepondues as $questionRepondue) {
            $reponse = $questionRepondue->getValeur();
            $words = explode(' ', $reponse);
            foreach ($words as $word) {
                if (!array_key_exists($word, $wordCounts)) {
                    $wordCounts[$word] = 0;
                }
                $wordCounts[$word]++;
            }
        }
        arsort($wordCounts); // Trie le tableau associatif par ordre décroissant de valeurs
        $mostCommonWord = array_key_first($wordCounts); // Récupère la clé (le mot) avec la plus grande valeur (occurrence)
        return $mostCommonWord;
    }

    public function getMotQuiRevientLeMoins(): string
    {
        $questionRepondues = $this->getQuestion()->getQuestionRepondues();
        $wordCounts = [];
        foreach ($questionRepondues as $questionRepondue) {
            $reponse = $questionRepondue->getValeur();
            $words = explode(' ', $reponse);
            foreach ($words as $word) {
                if (!array_key_exists($word, $wordCounts)) {
                    $wordCounts[$word] = 0;
                }
                $wordCounts[$word]++;
            }
        }
        arsort($wordCounts); // Trie le tableau associatif par ordre décroissant de valeurs
        $mostCommonWord = array_key_last($wordCounts); // Récupère la clé (le mot) avec la plus grande valeur (occurrence)
        return $mostCommonWord;
    }

    public function getDateLaPlusAncienne(): string {
        $oldestDate = null;
        foreach ($this->question->getQuestionRepondues() as $questionRepondue) {
            $dateString = $questionRepondue->getValeur();
            $date = DateTime::createFromFormat('Y-m-d', $dateString);

            if (!$date) {
                continue; // Ignore invalid dates
            }

            if (!$oldestDate || $date < $oldestDate) {
                $oldestDate = $date;
            }
        }

        return $oldestDate->format("d F Y");
    }

    public function getDateLaPlusRecente(): string {
        $youngestDate = null;
        foreach ($this->question->getQuestionRepondues() as $questionRepondue) {
            $dateString = $questionRepondue->getValeur();
            $date = DateTime::createFromFormat('Y-m-d', $dateString);

            if (!$date) {
                continue; // Ignore invalid dates
            }

            if (!$youngestDate || $date > $youngestDate) {
                $youngestDate = $date;
            }
        }

        return $youngestDate->format("d F Y");
    }

    public function getMoyenneNombre(): string {
        $res = 0;
        $nbNombre = 0;
        $sum = 0;
        foreach ($this->question->getQuestionRepondues() as $questionRepondue){
            $nbNombre++;
            $sum += ((integer) $questionRepondue->getValeur());
        }
        return (string) $nbNombre > 0 ? $sum / $nbNombre : 0;
    }

    public function getMaxNombre(): string {
        $questionRepondues = $this->question->getQuestionRepondues();
        $max = 0;
        foreach ($questionRepondues as $questionRepondue){
            if (((integer) $questionRepondue->getValeur()) > $max){
                $max = $questionRepondue->getValeur();
            }
        }
        return $max;
    }

    public function getMinNombre(): string {
        $questionRepondues = $this->question->getQuestionRepondues();
        $min = null;
        foreach ($questionRepondues as $questionRepondue){
            if ($min == null || ((integer) $questionRepondue->getValeur()) < $min){
                $min = $questionRepondue->getValeur();
            }
        }
        return $min;
    }



    // METHODES POUR LES STATISTIQUES A HISTOGRAMME

    public function getRepartitionDesSexes(): array
    {
        $sexeRepartitionData = [
            'abcisse' => ['Homme', 'Femme', "Autres"],
            'ordonnees' => 'Pourcentage de répartition',
            'valeur' => [0, 0, 0],
        ];

        $utilisateurs = [];
        $reponses = $this->question->getReponses();
        foreach ($reponses as $reponse) {
            foreach ($reponse->getQuestionRepondues() as $questionRepondue) {
                $utilisateur = $questionRepondue->getUtilisateur();
                if (in_array($utilisateur->getId(), $utilisateurs)) {
                    // On ignore cet utilisateur s'il a déjà répondu
                    continue;
                }
                $utilisateurs[] = $utilisateur->getId();
                if ($utilisateur->getGenre() === 0) {
                    $sexeRepartitionData['valeur'][0]++;
                } elseif ($utilisateur->getGenre() === 1) {
                    $sexeRepartitionData['valeur'][1]++;
                } else {
                    $sexeRepartitionData['valeur'][2]++;
                }
            }
        }

        $totalReponses = count($utilisateurs);

        if ($totalReponses > 0) {
            $sexeRepartitionData['valeur'][0] = round($sexeRepartitionData['valeur'][0] / $totalReponses * 100);
            $sexeRepartitionData['valeur'][1] = round($sexeRepartitionData['valeur'][1] / $totalReponses * 100);
            $sexeRepartitionData['valeur'][2] = round($sexeRepartitionData['valeur'][2] / $totalReponses * 100);
        }

        return $sexeRepartitionData;
    }

    public function getRepartitionDesCodesPostaux(): array
    {
        $codesPostauxRepartitionData = [
            'abcisse' => [],
            'ordonnees' => 'Nombre d\'utilisateurs',
            'valeur' => [],
        ];

        $utilisateurs = [];
        $reponses = $this->question->getReponses();
        foreach ($reponses as $reponse) {
            foreach ($reponse->getQuestionRepondues() as $questionRepondue) {
                $utilisateur = $questionRepondue->getUtilisateur();
                if (in_array($utilisateur->getId(), $utilisateurs)) {
                    // On ignore cet utilisateur s'il a déjà répondu
                    continue;
                }
                $utilisateurs[] = $utilisateur->getId();
                $codePostal = $utilisateur->getCodePostal();
                if (!in_array($codePostal, $codesPostauxRepartitionData['abcisse'])) {
                    $codesPostauxRepartitionData['abcisse'][] = $codePostal;
                    $codesPostauxRepartitionData['valeur'][] = 0;
                }
                $index = array_search($codePostal, $codesPostauxRepartitionData['abcisse']);
                $codesPostauxRepartitionData['valeur'][$index]++;
            }
        }

        return $codesPostauxRepartitionData;
    }

    public function getRepartitionDesReponses(): array
    {
        $reponseRepartitionData = [
            'abcisse' => [], // Les choix possibles de la question
            'ordonnees' => 'Nombre d\'occurrences',
            'valeur' => [],
        ];

        $reponses = $this->question->getReponses();
        foreach ($reponses as $reponse) {
            foreach ($reponse->getQuestionRepondues() as $questionRepondue) {
                $reponseQuestion = $questionRepondue->getReponse()->getIntitule();
                if (!in_array($reponseQuestion, $reponseRepartitionData['abcisse'])) {
                    // On ajoute le choix s'il n'existe pas encore dans la liste
                    $reponseRepartitionData['abcisse'][] = $reponseQuestion;
                    $reponseRepartitionData['valeur'][] = 0;
                }
                $indexChoix = array_search($reponseQuestion, $reponseRepartitionData['abcisse']);
                $reponseRepartitionData['valeur'][$indexChoix]++;
            }
        }

        return $reponseRepartitionData;
    }

    public function getTop10Mots(): array
    {
        $wordsCount = [];

        // Itérations sur les question-reponses pour extraire les mots des valeurs
        foreach ($this->question->getQuestionRepondues() as $questionRepondue) {
            $value = $questionRepondue->getValeur();
            if ($value !== null) {
                $words = str_word_count($value, 1);
                foreach ($words as $word) {
                    if (!array_key_exists($word, $wordsCount)) {
                        $wordsCount[$word] = 0;
                    }
                    $wordsCount[$word]++;
                }
            }
        }
        // Tri des mots par ordre décroissant du nombre d'occurrences
        arsort($wordsCount);

        // Récupération des 10 premiers mots
        $top10Words = array_slice($wordsCount, 0, 10, true);

        // Formatage des données pour la sortie
        $reponseRepartitionData = [
            'abcisse' => array_keys($top10Words),
            'ordonnees' => 'Nombre d\'occurrences',
            'valeur' => array_values($top10Words),
        ];

        return $reponseRepartitionData;
    }
}
