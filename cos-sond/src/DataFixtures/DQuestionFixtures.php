<?php

namespace App\DataFixtures;

use App\Entity\Question;
use App\Entity\Reponse;
use App\Entity\Sondage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class DQuestionFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');

        $listeSondages = $manager->getRepository(Sondage::class)->findAll();
//        echo $listeSondages[0]->getTheme();
        foreach ($listeSondages as $sondage) {
            for ($i = 1; $i <= 10; $i++) {
                $question = new Question();
                $question->setTitreQuestion('Question ' . $i);
                $question->setSondage($sondage);
                $typeQuestion = $faker->randomElement(['input', 'nombre', 'date', 'note', 'qcm', 'qcs']);
                $question->setTypeQuestion($typeQuestion);
                $question->setEstOptionnelle("false");
                $question->setNumeroQuestion($i);
                $manager->persist($question);
                $manager->flush();

                // Si la question est de type qcm ou qcs, on crée 3 réponses associées
                if (in_array($question->getTypeQuestion(), ['qcm', 'qcs'])) {
                    for ($j = 1; $j <= 3; $j++) {
                        $reponse = new Reponse();
                        $reponse->setIntitule('Réponse ' . $j . ' pour la question ' . $i);
                        $reponse->setQuestion($question);
                        $manager->persist($reponse);
                        $manager->flush();

                    }
                } else {
                    // Si la question est d'un autre type, on crée une seule réponse
                    $reponse = new Reponse();
                    $reponse->setIntitule($typeQuestion);
                    $reponse->setQuestion($question);
                    $manager->persist($reponse);
                    $manager->flush();

                }
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [CSondageFixtures::class, BProduitFixtures::class, AUserFixtures::class];
    }
}
