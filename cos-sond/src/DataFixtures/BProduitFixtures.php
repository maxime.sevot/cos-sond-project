<?php

namespace App\DataFixtures;

use App\Entity\Produit;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BProduitFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $lotion = new Produit();
        $lotion->setNom("Lotion");
        $lotion->setImage("lotion.jpg");
        $manager->persist($lotion);
        $product = new Produit();
        $product->setNom("Stick à lèvres");
        $product->setImage("stick.jpg");
        $manager->persist($product);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [AUserFixtures::class];
    }
}
