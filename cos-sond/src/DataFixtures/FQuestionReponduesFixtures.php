<?php

namespace App\DataFixtures;

use App\Entity\Question;
use App\Entity\QuestionRepondue;
use App\Entity\Reponse;
use App\Entity\Sondage;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class FQuestionReponduesFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');

        $listeSondage = $manager->getRepository(Sondage::class)->findAll();
        $listeUser = $manager->getRepository(User::class)->findAll();
        foreach ($listeUser as $user) {
            foreach ($listeSondage as $sond) {
                foreach ($sond->getQuestions() as $question) {
                    $questionRepondu = new QuestionRepondue();
                    $questionRepondu->setUtilisateur($user);
                    // Récupérer toutes les réponses possibles pour la question
//                    $reponses = $question->getReponses();
                    $reponses = $manager->getRepository(Reponse::class)->findBy(['question' => $question]);
                    // Si la question est de type qcm ou qcs, choisir une réponse au hasard
                    if ($question->getTypeQuestion() === 'qcm' || $question->getTypeQuestion() === 'qcs') {
                        $randomIndex = array_rand($reponses);
                        $reponseChoisie = $reponses[$randomIndex];
                        $questionRepondu->setReponse($reponseChoisie);
                        $questionRepondu->setValeur($reponseChoisie->getIntitule());
                    } else {
                        // Si la question est d'un autre type, choisir une valeur aléatoire avec Faker
                        $faker = Faker\Factory::create();
                        switch ($question->getTypeQuestion()) {
                            case 'input':
                                $text = $faker->words(10, true);
                                $questionRepondu->setValeur($text);

                                break;
                            case 'nombre':
                                $questionRepondu->setValeur($faker->numberBetween(1, 100));
                                break;
                            case 'date':
                                $questionRepondu->setValeur(($faker->dateTimeBetween('-1 year', 'now'))->format("Y-d-m"));
                                break;
                            case 'note':
                                $questionRepondu->setValeur($faker->numberBetween(1, 5));
                                break;
                            default:
                                break;
                        }
                        $questionRepondu->setReponse($question->getReponses()[0]);

                    }
                    $manager->persist($questionRepondu);
                }

            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [DQuestionFixtures::class, CSondageFixtures::class, AUserFixtures::class, BProduitFixtures::class];
    }
}
