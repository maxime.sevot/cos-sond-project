<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class AUserFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 100; $i++) {
            $user = new User();
            $nom = $faker->lastName;
            $prenom = $faker->firstName;
            $user->setNom($nom)
                ->setPrenom($prenom)
                ->setRoles(["ROLE_PARTICIPANT"])
                ->setEmail($prenom . "." . $nom . "@jaquieetmichel.com")
                ->setPassword("$2y$13$82NjLdmbrWdRAYVRd3Lvwe4L6p44hhMUwZszSj3.JGixeJrUMhZHS")
                ->setDateNaissance($faker->dateTimeBetween('-60 years', '-18 years'))
                ->setGenre($faker->randomElement([0, 1, 2]))
                ->setCodePostal($faker->numberBetween(10000, 99999));
            $manager->persist($user);
        }
        $manager->flush();
    }
}
