<?php

namespace App\DataFixtures;

use App\Entity\Produit;
use App\Entity\Question;
use App\Entity\QuestionRepondue;
use App\Entity\Reponse;
use App\Entity\Sondage;
use App\Entity\User;
use App\Repository\QuestionRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class ZFixturesCosSond extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');
        $jmc = new User();
        $jmc->setNom("Couvreur");
        $jmc->setPrenom("Jean-Michel")->setRoles(["ROLE_ADMIN", "ROLE_PARTICIPANT", "ROLE_SONDEUR"])->setEmail("jmc@univ-orleans.fr")->setGenre(0)->setCodePostal(69000)->setDateNaissance(new \DateTime())->setPassword("$2y$13$82NjLdmbrWdRAYVRd3Lvwe4L6p44hhMUwZszSj3.JGixeJrUMhZHS");
        $manager->persist($jmc);
        $manager->flush();
    }
}
