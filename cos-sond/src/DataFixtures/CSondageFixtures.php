<?php

namespace App\DataFixtures;

use App\Entity\Produit;
use App\Entity\Sondage;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class CSondageFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');

        $jmc = $manager->getRepository(User::class)->findOneBy(["email" => "jmc@univ-orleans.fr"]);
        $lotion = $manager->getRepository(Produit::class)->findOneBy(["nom" => "Lotion"]);
        $product = $manager->getRepository(Produit::class)->findOneBy(["nom" => "Stick à lèvre"]);
        $sondage1 = new Sondage();
        $sondage1->setCreateur($jmc)->setEstPublie(true)->setProduit($lotion)->setDateCreation(new \DateTime())->setDateFin($faker->dateTimeBetween('+1 years', '+18 years'))->setTheme("Lotion");
        $manager->persist($sondage1);
        $sondage2 = new Sondage();
        $sondage2->setCreateur($jmc)->setEstPublie(true)->setProduit($product)->setDateCreation(new \DateTime())->setDateFin($faker->dateTimeBetween('+1 years', '+18 years'))->setTheme("Stick à lèvre");
        $manager->persist($sondage2);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [BProduitFixtures::class, AUserFixtures::class];
    }
}
