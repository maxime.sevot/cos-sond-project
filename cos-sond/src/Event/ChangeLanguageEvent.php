<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

class ChangeLanguageEvent extends Event
{
    protected $locale;

    public function __construct(string $locale)
    {
        $this->locale = $locale;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }
}
