-- la liste des utilisateurs qui ont répondu à la question 'BLABLA' :
select * from utilisateur natural join (
    select idutilisateur from REPONDRE natural join (
        select idreponse from REPONSE natural join (
            select idquestion from QUESTION where titrequestion = 'BLABLA'
        )as foo3 
    ) as foo2) 
as djeloul;

-- Nombre de d'utilisateur qui ont répondu à un sondage :
SELECT COUNT()
FROM (
  SELECT DISTINCT
  FROM utilisateur
) AS foo2
NATURAL JOIN Repondre
NATURAL JOIN Reponse
NATURAL JOIN Question
NATURAL JOIN (
  SELECT *
  FROM Sondage
  WHERE idSondage = 3
) AS foo;

-- Nombre de réponse pour une réponse donnée :
SELECT COUNT( * )
FROM utilisateur
NATURAL JOIN (
  SELECT  *
  FROM Repondre
  WHERE idreponse = 10
) AS foo3
NATURAL JOIN (
  SELECT  *
  FROM Reponse
  WHERE idquestion = 5 
) AS foo
NATURAL JOIN QUESTION
NATURAL JOIN (
  SELECT idsondage
  FROM Sondage
  WHERE idSondage = 2
) AS foo4;


-- Pourcentage d'homme et de femme :
SELECT 
  ROUND(100.0 * SUM(CASE WHEN genre = 0 THEN 1 ELSE 0 END) / COUNT(*), 2) AS pourcentage_homme,
  ROUND(100.0 * SUM(CASE WHEN genre = 1 THEN 1 ELSE 0 END) / COUNT(*), 2) AS percentage_femme
FROM UTILISATEUR;

-- Age d'un utilisateur :
SELECT EXTRACT(YEAR FROM AGE(datenaiss)) from utilisateur where prenom = 'Corentin' and nom = 'Legeay';

-- Moyenne d'age de tous les utilisateurs :
SELECT AVG(EXTRACT(YEAR FROM AGE(datenaiss))) as average_age FROM UTILISATEUR;

-- Donne les statistiques que "Maxime" a fait :
SELECT * from UTILISATEUR natural join STATISTIQUE where prenom = 'Maxime';

-- Donne le temps restant avant la fin d'un sondage :
SELECT AGE(datedefin, current_date) FROM Sondage WHERE idSondage = 1;
