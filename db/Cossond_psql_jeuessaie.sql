insert into UTILISATEUR values (1, 'Sevot', 'Maxime', 'max@gmail.com', '123', 1, current_date, 0, 41600);
insert into UTILISATEUR values (2, 'Ottmann', 'Emma', 'emma@gmail.com', '123', 1, current_date, 1, 45000);
insert into UTILISATEUR values (3, 'Dubourjal', 'Tom', 'tom@gmail.com', '123', 0, current_date, 0, 27000);
insert into UTILISATEUR values (4, 'Legeay', 'Corentin', 'coco@gmail.com', '123', 0, current_date, 0, 27000);
insert into UTILISATEUR values (5, 'Musk', 'Elon', 'elon@tesla.com', '123', 0, current_date, 0, 11111);
insert into UTILISATEUR values (6, 'Poutine', 'Vladimir', 'poutine@gmail.ru', '123', 0, current_date, 0, 22222);

-- id, theme, dateCreation, dateFin, idUtilisateur
insert into SONDAGE values (1, 'Taille Corporelle', current_date, current_date+1, 1);
insert into SONDAGE values (2, 'Les meilleurs guèrre mondiales', current_date, current_date+1, 1);
insert into SONDAGE values (3, 'Les produits creacom', current_date, current_date+1, 2);

-- id, etsOptionnelle, typeQuestion, titre, numéro question, idsondage
insert into QUESTION VALUES (1, true, 'input', 'Votre taille en cm ?', 1, 1);
insert into QUESTION VALUES (2, true, 'input', 'Votre taille en mètre ?', 2, 1);
insert into QUESTION VALUES (3, true, 'input', 'Taille de votre index droit ?', 3, 1);
insert into QUESTION VALUES (4, true, 'QCM', 'Quelle est votre parfum préféré ?', 1, 2);
insert into QUESTION VALUES (5, true, 'QCS', 'Quantité préférée', 2, 2);
insert into QUESTION VALUES (6, true, 'input', 'Combien de produit avez vous ?', 1, 3);
insert into QUESTION VALUES (7, true, 'input', 'Votre produit préféré ?', 2, 3);
insert into QUESTION VALUES (8, true, 'QCS', 'Acheteriez vous plus de produits ?', 3, 3);
insert into QUESTION VALUES (9, true, 'QCM', 'Qu est ce qui est important pour vous dans un produit ?', 4, 3);

-- id, intitule, id question à laquelle elle répond
insert into REPONSE VALUES (1, 'float', 1);
insert into REPONSE VALUES (2, 'float', 2);
insert into REPONSE VALUES (3, 'float', 3);
insert into REPONSE VALUES (4, 'Pior Jadore', 4);
insert into REPONSE VALUES (5, 'YS Cocoraiban', 4);
insert into REPONSE VALUES (6, 'Fiona Ricchie', 4);
insert into REPONSE VALUES (7, '10ml', 5);
insert into REPONSE VALUES (8, '15ml', 5);
insert into REPONSE VALUES (9, '33cl', 5);
insert into REPONSE VALUES (10, '1l', 5);
insert into REPONSE VALUES (11, 'number', 6);
insert into REPONSE VALUES (12, 'texte', 7);
insert into REPONSE VALUES (13, 'Oui', 8);
insert into REPONSE VALUES (14, 'Non', 8);
insert into REPONSE VALUES (15, 'Peut-être', 8);
insert into REPONSE VALUES (16, 'Quil soit bio', 9);
insert into REPONSE VALUES (17, 'Bonne odeur', 9);
insert into REPONSE VALUES (18, 'Pas chère', 9);
insert into REPONSE VALUES (19, 'Le flacon', 9);
insert into REPONSE VALUES (20, 'Le nom', 9);
insert into REPONSE VALUES (21, 'Fabriqué par des hommes', 9);

-- id de la réponse utilisée, id de l'utilisateur qui y a répondu, réponse de l'utilisateur
-- Premier sondage de Maxime
insert into REPONDRE VALUES (1, 1, '188');
insert into REPONDRE VALUES (1, 2, '169');
insert into REPONDRE VALUES (1, 3, '178');
insert into REPONDRE VALUES (1, 4, '179');
insert into REPONDRE VALUES (1, 5, '186');

insert into REPONDRE VALUES (2, 1, '1,88');
insert into REPONDRE VALUES (2, 2, '1,69');
insert into REPONDRE VALUES (2, 3, '1,78');
insert into REPONDRE VALUES (2, 4, '1,79');
insert into REPONDRE VALUES (2, 5, '1,86');

insert into REPONDRE VALUES (3, 1, '3');
insert into REPONDRE VALUES (3, 3, '14');
insert into REPONDRE VALUES (3, 4, '21');
insert into REPONDRE VALUES (3, 5, '16');

-- Deuxième sondage de Maxime
insert into REPONDRE VALUES (4, 1, null);
insert into REPONDRE VALUES (5, 1, null);
insert into REPONDRE VALUES (4, 3, null);
insert into REPONDRE VALUES (4, 4, null);
insert into REPONDRE VALUES (6, 4, null);

insert into REPONDRE VALUES (8, 1, null);
insert into REPONDRE VALUES (10, 3, null);
insert into REPONDRE VALUES (10, 4, null);

-- Troisème sonadage de Emma
insert into REPONDRE VALUES (11, 2, '25');
insert into REPONDRE VALUES (11, 3, '3');
insert into REPONDRE VALUES (11, 4, '0');
insert into REPONDRE VALUES (11, 6, '8');

insert into REPONDRE VALUES (12, 2, 'Phare à paupière Lancôme');
insert into REPONDRE VALUES (12, 3, 'Les rinces doigts Yves Rocher');
-- Corentin ne répond pas à la question
insert into REPONDRE VALUEs (12, 6, 'Les lèvres Fior');

insert into REPONDRE VALUES (13, 2, 'Oui');
insert into REPONDRE VALUES (13, 3, 'Oui');
insert into REPONDRE VALUES (14, 4, 'Non');
insert into REPONDRE VALUES (14, 6, 'Peut-être');

insert into REPONDRE VALUES (16, 2, null);
insert into REPONDRE VALUES (17, 2, null);
insert into REPONDRE VALUES (16, 3, null);
insert into REPONDRE VALUES (19, 3, null);
insert into REPONDRE VALUES (20, 3, null);
insert into REPONDRE VALUES (16, 4, null);
insert into REPONDRE VALUES (19, 4, null);
insert into REPONDRE VALUES (20, 4, null);
insert into REPONDRE VALUES (21, 4, null);
insert into REPONDRE VALUES (16, 6, null);
insert into REPONDRE VALUES (18, 6, null);
insert into REPONDRE VALUES (21, 6, null);


INSERT INTO STATISTIQUE VALUES (1, 'select count(*) from reponse where idquestion = 1', 1, 1); -- Combien de réponse à la question d'id 1
INSERT INTO STATISTIQUE VALUES (2, 'select count(*) from reponse where idquestion = 3', 2, 6); -- Combien de réponse à la question d'id 2
INSERT INTO STATISTIQUE VALUES (3, 'select count(*) from reponse where idquestion = 2', 2, 9); -- Combien de réponse à la question d'id 3

-- INSERT INTO PREDICAT VALUES (1, 'reponse', '>', '1.90', 1);