-- CREATE DATABASE COSSOND;
\c COSSOND;

drop table STATISTIQUE;
drop table REPONDRE;
drop table REPONSE;
drop table QUESTION;
drop table SONDAGE;
drop table UTILISATEUR;

CREATE TABLE UTILISATEUR (
  idutilisateur int,
  nom VARCHAR(42),
  prenom VARCHAR(42),
  mail VARCHAR(42),
  mdp VARCHAR(42),
  roleuser int,
  datenaiss DATE,
  genre int,
  codepostale int,
  CONSTRAINT PK_utilisateur PRIMARY KEY (idutilisateur)
);

CREATE TABLE SONDAGE (
  idsondage int,
  theme VARCHAR(42),
  datecreation Date,
  datedefin DATE,
  idutilisateur int,
  CONSTRAINT PK_sondage PRIMARY KEY (idsondage),
  CONSTRAINT FK_sondage_utilisateur FOREIGN KEY (idutilisateur) REFERENCES UTILISATEUR(idutilisateur)
);

CREATE TABLE QUESTION (
  idquestion int,
  estoptionnelle Boolean,
  typequestion VARCHAR(42),
  titrequestion VARCHAR(200),
  numquestion int,
  idsondage int,
  CONSTRAINT PK_question PRIMARY KEY (idquestion),
  CONSTRAINT FK_question_sondage FOREIGN KEY (idsondage) REFERENCES SONDAGE(idsondage)
);

CREATE TABLE REPONSE (
  idreponse int,
  intitule VARCHAR(42),
  idquestion int,
  CONSTRAINT PK_reponse PRIMARY KEY (idreponse),
  CONSTRAINT FK_reponse_question FOREIGN KEY (idquestion) REFERENCES QUESTION(idquestion)
);

CREATE TABLE REPONDRE (
  idreponse int,
  idutilisateur int,
  reponse VARCHAR(42),
  CONSTRAINT PK_repondre PRIMARY KEY (idreponse, idutilisateur),
  CONSTRAINT FK_repondutilisateur FOREIGN KEY (idutilisateur) REFERENCES UTILISATEUR(idutilisateur)
);

CREATE TABLE STATISTIQUE (
  idstatistique int,
  requete VARCHAR(500),
  idutilisateur int,
  idquestion int,
  CONSTRAINT PK_statistique PRIMARY KEY (idstatistique),
  CONSTRAINT FK_statistique_utilisateur FOREIGN KEY (idutilisateur) REFERENCES UTILISATEUR(idutilisateur),
  CONSTRAINT FK_statistique_question FOREIGN KEY (idquestion) REFERENCES QUESTION(idquestion)
);
