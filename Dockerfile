FROM library/postgres
WORKDIR /code
ENV POSTGRES_USER miage
ENV POSTGRES_PASSWORD miage
ENV POSTGRES_DB cossond
COPY . .
EXPOSE 800
CMD ["\i", "Base de données/createTable.sql"]
